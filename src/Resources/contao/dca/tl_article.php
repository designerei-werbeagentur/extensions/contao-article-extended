<?php

// subpalettes
$GLOBALS['TL_DCA']['tl_article']['palettes']['__selector__'][] = 'addImage';
$GLOBALS['TL_DCA']['tl_article']['subpalettes']['addImage'] = 'singleSRC, size, backgroundOverlayClass';

// fields
$GLOBALS['TL_DCA']['tl_article']['fields']['articleDimension'] = array
(
    'exclude'                 => true,
    'inputType'               => 'text',
    'default'                 => 'a:4:{i:0;s:5:"pt-24";i:1;s:4:"pr-6";i:2;s:5:"pb-24";i:3;s:4:"pl-6";}',
    'eval'                    => array('multiple'=>true, 'size'=>4, 'decodeEntities'=>true, 'tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
    // a:4:{i:0;s:5:"pt-24";i:1;s:4:"pr-6";i:2;s:5:"pb-24";i:3;s:4:"pl-6";}
);

$GLOBALS['TL_DCA']['tl_article']['fields']['articleWrapper'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'default'                 => 'max-w-screen-md',
    'options'                 => array(
                                'max-w-screen-sm',
                                'max-w-screen-md',
                                'max-w-screen-lg',
                                'max-w-screen-xl',
                                'max-w-full'),
    'eval'                    => array('tl_class'=>'w50'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_article']['articleWrapper'],
    'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['backgroundColor'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array(
                                '',
                                'bg-light',
                                'bg-dark',
                                'bg-primary',
                                'bg-accent'),
    'eval'                    => array('tl_class'=>'w50'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_article']['backgroundColor'],
    'sql'                     => "varchar(32) default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['backgroundOverlayClass'] = array
(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['addImage'] = array
(
  'exclude'                 => true,
  'inputType'               => 'checkbox',
  'eval'                    => array('submitOnChange'=>true, 'tl_class'=>'clr'),
  'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['singleSRC'] = array
(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr'),
    'sql'                     => "binary(16) NULL"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['size'] = array
(
    'exclude'                 => true,
    'inputType'               => 'imageSize',
    'reference'               => &$GLOBALS['TL_LANG']['MSC'],
    'eval'                    => array('rgxp'=>'natural', 'includeBlankOption'=>true, 'nospace'=>true, 'helpwizard'=>true, 'tl_class'=>'w50'),
    'options_callback' => static function ()
    {
      return System::getContainer()->get('contao.image.image_sizes')->getOptionsForUser(BackendUser::getInstance());
    },
    'sql'                     => "varchar(255) NOT NULL default ''"
);

// paletteManipulator
use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
  ->addLegend('background_legend', 'expert_legend', PaletteManipulator::POSITION_BEFORE)
  ->addLegend('article_legend', 'expert_legend', PaletteManipulator::POSITION_BEFORE)
  ->addField('articleDimension', 'article_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('articleWrapper', 'article_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('backgroundColor', 'background_legend', PaletteManipulator::POSITION_APPEND)
  ->addField('addImage', 'background_legend', PaletteManipulator::POSITION_APPEND)
  ->applyToPalette('default', 'tl_article')
;
