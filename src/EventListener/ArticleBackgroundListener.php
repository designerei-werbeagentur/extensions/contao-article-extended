<?php

namespace designerei\ContaoArticleExtendedBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Contao\FilesModel;
use Contao\Controller;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ArticleBackgroundListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        if ($template->type === 'article') {

            if($template->backgroundColor) {
                $template->class .= ' ' . $template->backgroundColor;
            }

            if($template->addImage) {

                $image = FilesModel::findByUuid($template->singleSRC);

                Controller::addImageToTemplate($template, [
                    'singleSRC' => $image->path,
                    'size' => $template->size,
                ], null, null, $image);
            }
        }
    }
}
