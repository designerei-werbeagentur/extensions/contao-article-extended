<?php

namespace designerei\ContaoArticleExtendedBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Template;
use Terminal42\ServiceAnnotationBundle\ServiceAnnotationInterface;

class ArticleDimensionListener implements ServiceAnnotationInterface
{
    /**
     * @Hook("parseTemplate")
     */
    public function onParseTemplate(Template $template): void
    {
        if ($template->type === 'article') {

            // deserialize string
            $template->articleDimension = deserialize($template->articleDimension);

            $result = isset($result) ? $result : '';

            // convert array to string
            if(!empty($template->articleDimension))
            {
                foreach ($template->articleDimension as $value)
                {
                    if(!empty($value))
                    {
                        $result .= $value . ' ';
                    }
                }
            }

            // remove last whitespace from string and output result to template
            $template->articleDimension = rtrim($result);

            // extend $class with $articleDimension
            $template->class .= ' ' . $template->articleDimension;
        }
    }
}
